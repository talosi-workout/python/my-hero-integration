from services.sql_service import get_sql_template
from services.avengers import get_avengers_details


sql_template = get_sql_template()
avangers_list_json = get_avengers_details()
table_name = "avengers_table"

def build_sql_from_json():
    sql_list = []
    for elt in avangers_list_json:
        columns_name = ""
        values_data= ""
        for key in elt.keys():
            columns_name += "\"" + key + "\" ," 
            values_data += "\"" + elt[key] + "\" ," 

        sql_list.append(sql_template.format(table=table_name, 
                                values=values_data[:-1], 
                                col= columns_name[:-1])
                                )
    return sql_list
                                
