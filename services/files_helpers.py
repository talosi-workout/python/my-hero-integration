import json

def read_json(filename):
    with open(filename) as mon_fichier:
        data = json.load(mon_fichier)
    return data

def read_sql(filename):
    with open(filename) as mon_fichier:
        data = mon_fichier.read()
    return data