from services.files_helpers import read_sql

filename="./resources/template.sql"

def get_sql_template():
    return read_sql(filename)
