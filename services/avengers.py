from services.files_helpers import read_json

filename="./resources/avengers.json"

def get_avengers_details():
    return read_json(filename)
